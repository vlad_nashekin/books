﻿using Nager.ArticleNumber;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kt5.Tests
{
    [TestClass]
    public class IsinValidatorTests
    {
        [TestMethod]
        public void ShouldValidateCorrectIsbn_1()
        {
            var s = "978-0-123456-47-2";
            Assert.True(ArticleNumberHelper.IsValidIsbn13(s));
        }
    }
}
