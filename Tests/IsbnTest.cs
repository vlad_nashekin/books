﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nager.ArticleNumber;

namespace Tests
{
    [TestClass]
    public class IsbnTest
    {
        [TestMethod]
        public void ShouldValidateCorrectIsbn_1()
        {
            Assert.IsTrue(ArticleNumberHelper.IsValidIsbn13("978-0-123456-47-2"));
        }

        [TestMethod]
        public void ShouldValidateCorrectIsbn_2()
        {
            Assert.IsTrue(ArticleNumberHelper.IsValidIsbn13("9780123456-47-2"));
        }

        [TestMethod]
        public void ShouldNotValidateIncorrectIsbn()
        {
            Assert.IsFalse(ArticleNumberHelper.IsValidIsbn13("8780123456-47-2"));
        }
    }
}