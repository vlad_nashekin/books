﻿import {Component, Inject,Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Book,ApiResult} from './models/model';

@Injectable()
export class BookService {

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
    
    }

    parseData<T>(res: any): ApiResult<T>{
        let json = res.json() as ApiResult<T>;
        return json;
    }

    getBooks(): Observable<ApiResult<Book[]>> {
        return this.http.get(this.baseUrl + 'api/books/Books')
            .map(res => this.parseData<Book[]>(res))
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        let errorMessage: string;
        errorMessage = error.message ? error.message : error.toString();
        return Observable.throw(errorMessage);
    }

    

    editBook(book: Book): Observable<ApiResult<Book>> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions();
        options.headers = headers;


        return this.http.post(this.baseUrl + 'api/books/EditBook', book, options)
            .map(result => this.parseData<Book>(result))
            .catch(error => {
                console.log(error);
                return Observable.throw(error.json().error || 'Server error')
            });
    }

    insertBook(book: Book): Observable<ApiResult<Book>> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions();
        options.headers = headers;


        return this.http.post(this.baseUrl + 'api/books/InsertBook', book, options)
            .map(result => this.parseData<Book>(result))
            .catch(error => {
                console.log(error);
                return Observable.throw(error.json().error || 'Server error')
            });
    }

    deleteBook(book: Book): Observable<ApiResult<boolean>> {
        const url = this.baseUrl + 'api/books/DeleteBook?id=' + book.id;

        return this.http.delete(url)
            .map(result => this.parseData<boolean>(result))
            .catch(error => {
                console.log(error);
                return Observable.throw(error.json().error || 'Server error')
            });
    }
}

