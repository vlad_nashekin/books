﻿export class Book {
    public id: number;
    public title: string;
    public authors: Array<Author>;
    public pagesCount: number;
    public publisher: string;
    public publicationYear: number;
    public isbn: string;

    constructor() {
        this.id = 0;
        this.authors = [];
        this.authors.push(new Author());
    }
  

    get firstAuthor() {
        console.log(this.authors)
        return this.authors[0]
    }
}



export class Author {
    public firstName: string;
    public lastName: string;

    constructor() {
        this.firstName = '';
        this.lastName = '';
    }
    get fullName() {
        console.log(this.firstName)
        return this.firstName + ' ' + this.lastName;
    }

}

export class ApiResult<T>{
    public success: boolean;
    public errors: string[];
    public data: T;

}


export class DialogRes<T>{
    public success: boolean;
    public data: T;
}