﻿import { Component, Input, OnChanges, Inject, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Http, RequestOptions } from '@angular/http';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { BSModalContext } from 'ngx-modialog/plugins/bootstrap';
import {Book,Author} from '../../models/model'

export class BookModalContext extends BSModalContext {
    public num1: number;
    public num2: number;
    public book: Book;
}

@Component({
    selector: 'modal-content',
    styles: [`
        .custom-modal-container {
            padding: 15px;
        }

        .custom-modal-header {
            background-color: #219161;
            color: #fff;
            -webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.75);
            margin-top: -15px;
            margin-bottom: 40px;
        }
    `],
    templateUrl: './edit-book-modal.component.html'
})
export class EditBookModalComponent implements CloseGuard, ModalComponent<BookModalContext> {
    context: BookModalContext;

    constructor(public dialog: DialogRef<BookModalContext>, private http: Http, @Inject('BASE_URL') private baseUrl: string,
        private fb: FormBuilder) {
        this.context = dialog.context;
        
        dialog.setCloseGuard(this);
    }

   

    beforeDismiss(): boolean {
        return true;
    }

    beforeClose(): boolean {
        return false;
    }

    onSuccess(book: Book): void {
        console.log('success');
        this.dialog.close({ success: true, book: book });
    }

    onCancel(): void {
        console.log('cancel');
        this.dialog.close({ success: false});
    }
}


export class BookResult {
    public success: boolean;
    public book: Book;
}