﻿import { Component, Input, OnChanges, Inject, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { Http, RequestOptions } from '@angular/http';
import { DialogRef, ModalComponent, CloseGuard } from 'ngx-modialog';
import { Book, Author } from '../../models/model';
import { validateIsbn } from '../../isbn-validator';
import { BookService } from '../../book.service';
import { Modal, BSModalContext, BSModalContextBuilder } from 'ngx-modialog/plugins/bootstrap';


@Component({
    selector: 'edit-book-detail',
    templateUrl: './edit-book-detail.component.html',
    providers: [BookService]
})
export class EditBookDetailComponent implements OnChanges{
    @Input() book: Book;
    @Output() success = new EventEmitter();
    @Output() cancel = new EventEmitter();

    editBookForm: FormGroup;

    constructor(public modal: Modal,private http: Http, @Inject('BASE_URL') private baseUrl: string,
        private fb: FormBuilder, private bookService:BookService) {

        this.createForm();
    }

    createForm() {
        this.editBookForm = this.fb.group({
            id: 0,
            title: ['', Validators.required],
            authors: this.fb.array([]),
            pagesCount: [0, Validators.compose([Validators.required,Validators.min(1), Validators.max(10000)])],
            publisher: ['', Validators.maxLength(30)],
            publicationYear: [2017, Validators.compose([Validators.required,Validators.min(1799)])],
            isbn: ['', validateIsbn]
        });
    }

    get authors(): FormArray {
        return this.editBookForm.get('authors') as FormArray;
    };

    addAuthor() {
        this.authors.push(this.createAuthorForm(new Author()));
    }
    removeAuthor() {
        if (this.authors.length > 1) {
            this.authors.removeAt(this.authors.length - 1);
        }
    }

    createAuthorForm(author: Author) {
        return this.fb.group({
            firstName: [author.firstName, Validators.compose([Validators.required, Validators.maxLength(20)])],
            lastName: [author.lastName, Validators.compose([Validators.required, Validators.maxLength(20)])]
        })
    }

    setAuthors(authors: Author[]) {
        const authorFGs = authors.map(author => this.createAuthorForm(author));
        const authorFormArray = this.fb.array(authorFGs);
        this.editBookForm.setControl('authors', authorFormArray);
    }

    ngOnChanges() {
        this.editBookForm.reset({
            id: this.book.id,
            title: this.book.title,
            pagesCount: this.book.pagesCount,
            publisher: this.book.publisher,
            publicationYear: this.book.publicationYear,
            isbn: this.book.isbn
        });
        this.setAuthors(this.book.authors);
    }


    onSubmit() {
        this.book = this.prepareSaveBook();
        console.log(this.book);
        let editMode = this.book.id != 0;
        console.log('editMode', editMode);
        if (editMode) {
            this.bookService.editBook(this.book).subscribe(res => {
                if (res.success) {
                    console.log(res);
                    this.success.emit(res.data);
                } else {
                    this.showErrors(res.errors);
                }

            });
        } else {
            this.bookService.insertBook(this.book).subscribe(res => {
                if (res.success) {
                    console.log(res);
                    this.success.emit(res.data);
                } else {
                    this.showErrors(res.errors);
                }

            });
        }
        
        this.ngOnChanges();
    }

    cancelEdit() {
        this.cancel.emit();
    }


    showErrors(errors: string[]) {
        let lines = errors.join("\r\n");

        this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .keyboard(27)
            .title('Error')
            .body(lines)
            .open();
    }


    prepareSaveBook(): Book {
        const formModel = this.editBookForm.value;

        const authorsCopy: Author[] = formModel.authors.map(
            (author: Author) => Object.assign({}, author)
        );

        var saveBook: Book = Object.assign({}, formModel);
        saveBook.authors = authorsCopy;

        return saveBook;
    }
}