import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { Http } from '@angular/http';
import { overlayConfigFactory, OverlayConfig } from "ngx-modialog";
import { Modal, BSModalContext, BSModalContextBuilder } from 'ngx-modialog/plugins/bootstrap';
import { EditBookModalComponent, BookResult, BookModalContext } from '../edit-book/edit-book-modal.component';
import { AddBookModalComponent} from '../edit-book/add-book-modal.component';

import { BookService } from '../../book.service';
import { Book, Author } from '../../models/model';

@Component({
    selector: 'book-list',
    templateUrl: './book-list.component.html',
    providers: [BookService],
    encapsulation:ViewEncapsulation.None
})
export class BookListComponent {
    public books: Book[];

    constructor(public modal: Modal, private bookService: BookService) {
        bookService.getBooks().subscribe(result => {
            if (result.success) {
                console.log(result.data);
                this.books = result.data;
            } else {
                console.error(result.errors)
            }
        }, error => console.error(error));
    }

    onEditBook(event, book) {
        const dialogRef = this.modal
            .open(EditBookModalComponent, overlayConfigFactory({num1: 2, num2: 3, book: book },
                BSModalContext));

        dialogRef
            .then(resultPromise => {
                return resultPromise.result
                    .then(r=>this.handleUpdateResult(r),
                        () => console.log('error')
                    );
            });
    }

    onDeleteBook(event, book) {

        const dialogRef =this.modal.alert()
            .size('sm')
            .isBlocking(true)
            .showClose(true)
            .keyboard(27)
            .title('Delete book')
            .body('Are you sure you want delete book?')
            .open();
        
        
        dialogRef
            .then(resultPromise => {
                console.log(resultPromise);
                return resultPromise.result
                    .then(r => this.handleDelete(book),
                    () => console.log('error')
                    );
            });
    }

    onAddBook(event) {
        const dialogRef = this.modal
            .open(AddBookModalComponent, overlayConfigFactory({ num1: 2, num2: 3, book: new Book()},
                BSModalContext));
        dialogRef
            .then(resultPromise => {
                return resultPromise.result
                    .then(r => this.handleInsertResult(r),
                        () => console.log('error')
                    );
            });
    }

    handleDelete(book: Book) {
        this.bookService.deleteBook(book).subscribe(res => {
            if (res.success) {
                this.books = this.books.filter(b => b.id != book.id);
            } else {
                console.log(res.errors);
            }
        });
    }

    handleUpdateResult(result: BookResult) {
        if (result.success) {
            let book = result.book as Book;
            let target = this.books.find(x => x.id == book.id);
            if (target != null) {
                this.updateBook(book, target);
            }

        }
    }


    handleInsertResult(result: BookResult) {
        if (result.success) {
            let book = result.book as Book;
            this.books.push(book);
        }
    }

    updateBook(source: Book, target: Book) {
        target.title = source.title;
        target.publisher = source.publisher;
        target.publicationYear = source.publicationYear;
        target.isbn = source.isbn;
        target.pagesCount = source.pagesCount;
        target.authors = source.authors;
    }
}
