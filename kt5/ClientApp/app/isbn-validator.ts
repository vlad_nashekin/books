﻿
import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { FormControl } from '@angular/forms';

declare var ISBN: any;
export function validateIsbn(c: FormControl) {
    var passed = false;

    if (c.value != null && c.value != "") {
        console.log(c.value);
        let h = ISBN.hyphenate(c.value.replace(/-/g, ''));
        if (h) {
            let isbn = ISBN.parse(h);
            passed = isbn != null && isbn.isIsbn13();
        }
        
    }
    
    return passed ? null : {
        validateIsbn: {
            valid: passed
        }
    };
}


@Directive({
    selector: '[validateIsbn][ngModel]',
    providers: [
        { provide: NG_VALIDATORS, useValue: validateIsbn, multi: true }
    ]
})
export class IsbnValidator { }