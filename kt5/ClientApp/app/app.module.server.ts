import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppModuleShared } from './app.module.shared';
import { AppComponent } from './components/app/app.component';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { EditBookModalComponent } from './components/edit-book/edit-book-modal.component';
import { AddBookModalComponent } from './components/edit-book/add-book-modal.component';

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        ServerModule,
        AppModuleShared,
        ModalModule.forRoot(),
        BootstrapModalModule
    ],
    entryComponents: [
        EditBookModalComponent,
        AddBookModalComponent
    ]
})
export class AppModule {
}
