import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { CustomFormsModule } from 'ng2-validation'

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';

import { EditBookModalComponent } from './components/edit-book/edit-book-modal.component';
import { AddBookModalComponent } from './components/edit-book/add-book-modal.component';

import { EditBookDetailComponent } from './components/edit-book/edit-book-detail.component';
import { IsbnValidator } from './isbn-validator';
import { BookListComponent} from "./components/book-list/book-list.component";


@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        BookListComponent,
        EditBookModalComponent,
        AddBookModalComponent,
        EditBookDetailComponent,
        IsbnValidator
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        CustomFormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'books', pathMatch: 'full' },
            { path: 'books', component: BookListComponent },
            { path: '**', redirectTo: 'books' }
        ])
    ]
})
export class AppModuleShared {
}
