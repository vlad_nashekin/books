import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.module.shared';
import { AppComponent } from './components/app/app.component';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';
import { EditBookModalComponent } from './components/edit-book/edit-book-modal.component';
import { AddBookModalComponent } from './components/edit-book/add-book-modal.component';

@NgModule({
    bootstrap: [ AppComponent ],
    imports: [
        BrowserModule,
        AppModuleShared,
        ModalModule.forRoot(),
        BootstrapModalModule
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }
    ],
    entryComponents: [
        EditBookModalComponent,
        AddBookModalComponent
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
