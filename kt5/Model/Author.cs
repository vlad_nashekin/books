﻿namespace kt5.Model
{
    public class Author
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}