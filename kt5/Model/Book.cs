﻿using System.Collections.Generic;

namespace kt5.Model
{
    public class Book
    {
        public Book()
        {
            Authors = new List<Author>();
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public int PagesCount { get; set; }

        public string Publisher { get; set; }

        public int PublicationYear { get; set; }

        public string Isbn { get; set; }

        public List<Author> Authors { get; set; }

    }
}
