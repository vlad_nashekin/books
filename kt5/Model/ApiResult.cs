﻿using System.Collections.Generic;
using System.Linq;

namespace kt5.Model
{
    public class ApiResult<T>
    {
        public string[] Errors { get; set; }

        public bool Success { get; set; }

        public T Data { get; set; }

        public static ApiResult<T> FromSuccess(T data)
        {
            return new ApiResult<T>()
            {
                Success = true,
                Data = data
            };
        }

        public static ApiResult<T> FromError(string errors)
        {
            return new ApiResult<T>()
            {
                Success = false,
                Errors = new string[] {errors}
            };
        }

        public static ApiResult<T> FromError(IEnumerable<string> errors)
        {
            return new ApiResult<T>()
            {
                Success = false,
                Errors = errors.ToArray()
            };
        }
    }
}
