﻿using kt5.Model;
using System.Collections.Generic;
using System.Linq;

namespace kt5.Components
{
    public class InMemoryBookRepository : IBookRepository
    {
        private static List<Book> _books;
        private readonly object _lock = new object();
        private int _lastId;

        public InMemoryBookRepository()
        {
            _books = new List<Book>
            {
                new Book()
                {
                    Id = 1,
                    Title = "Ulysses",
                    PagesCount = 228,
                    Publisher ="CreateSpace Independent",
                    PublicationYear =2013,
                    Isbn ="978-1494405496",
                    Authors = new List<Author>
                    {
                        new Author
                        {
                            FirstName = "James",
                            LastName = "Joyce"
                        }
                    }
                },
                new Book()
                {
                    Id  = 2,
                    Title = "Options, Futures, and Other D.",
                    PagesCount = 896,
                    Publisher ="Pearson",
                    PublicationYear =2014,
                    Isbn ="978-0133456318",
                    Authors = new List<Author>
                    {
                        new Author
                        {
                            FirstName = "John",
                            LastName = "Hull"
                        }
                    }

                },
                new Book()
                {
                    Id = 3,
                    Title = "Book with two authors",
                    PagesCount = 100,
                    Publisher ="Publisher",
                    PublicationYear =1910,
                    Isbn ="978-0030867323",
                    Authors = new List<Author>
                    {
                        new Author
                        {
                            FirstName = "FirstName1",
                            LastName = "LastName1"
                        },
                        new Author
                        {
                            FirstName = "FirstName2",
                            LastName = "LastName2"
                        }
                    }
                }
            };

            _lastId = _books.Max(x => x.Id);
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _books;
        }

        public bool TryEditBook(Book book)
        {
            lock (_lock)
            {
                var existing = _books.FirstOrDefault(x => x.Id == book.Id);
                if(existing != null)
                {
                        _books.Remove(existing);
                        _books.Add(book);
                        return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool TryInsertBook(Book book)
        {
            lock (_lock)
            {
                _lastId = _lastId + 1;
                book.Id = _lastId;
                _books.Add(book);
                return true;
            }
        }

        public void DeleteBook(int id)
        {
            lock (_lock)
            {
                var book = _books.FirstOrDefault(x => x.Id == id);
                if(book != null)
                {
                    _books.Remove(book);
                }
            }
        }
    }
}
