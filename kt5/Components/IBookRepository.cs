﻿using kt5.Model;
using System.Collections.Generic;


namespace kt5.Components
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAllBooks();
        bool TryInsertBook(Book book);
        bool TryEditBook(Book book);
        void DeleteBook(int id);
    }
}
