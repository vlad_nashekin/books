﻿using FluentValidation;
using kt5.Model;


namespace kt5.Components
{
    public class AuthorValidator: AbstractValidator<Author>
    {
        public AuthorValidator()
        {
            RuleFor(a => a.FirstName).NotEmpty().WithMessage("First name is required (maximum 20 characters).");
            RuleFor(a => a.FirstName).MaximumLength(20).WithMessage("First name is required (maximum 20 characters).");

            RuleFor(a => a.LastName).MaximumLength(20).WithMessage("First name is required (maximum 20 characters).");
            RuleFor(a => a.LastName).NotEmpty().WithMessage("First name is required (maximum 20 characters).");
        }
    }
}
