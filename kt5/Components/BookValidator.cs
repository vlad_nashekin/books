﻿using FluentValidation;
using kt5.Model;
using Nager.ArticleNumber;


namespace kt5.Components
{
    public class BookValidator: AbstractValidator<Book>
    {
        public BookValidator()
        {
            RuleFor(b => b.Title).NotEmpty().WithMessage("Title is required (maximum 30 characters).");
            RuleFor(b => b.Title).MaximumLength(30).WithMessage("Title is required (maximum 30 characters).");

            RuleFor(b => b.PagesCount).GreaterThan(1).WithMessage("The number of pages should be between 1 and 10000.");
            RuleFor(b => b.PagesCount).LessThanOrEqualTo(10000).WithMessage("The number of pages should be between 1 and 10000.");

            RuleFor(b => b.Publisher).Must(p =>
            {
                if (!string.IsNullOrWhiteSpace(p) && p.Length > 30)
                {
                    return false;
                }
                return true;
            }).WithMessage("Publisher is too long (maximum 30 characters).");

            RuleFor(b => b.PublicationYear).GreaterThanOrEqualTo(1800).WithMessage("Year of publication must be after 1799.");

            RuleFor(b => b.Isbn).Must(ArticleNumberHelper.IsValidIsbn13).WithMessage("ISBN is not valid.");

            RuleFor(b => b.Authors).NotEmpty().WithMessage("At least one author is required.");
            RuleFor(b => b.Authors).SetCollectionValidator(new AuthorValidator());

        }
    }
}
