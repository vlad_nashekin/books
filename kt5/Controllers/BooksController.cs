using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kt5.Components;
using kt5.Model;

namespace kt5.Controllers
{
    [Route("api/[controller]")]
    public class BooksController : Controller
    {
        private readonly IBookRepository _bookRepository;

        public BooksController(IBookRepository bookRepository)
        {
            this._bookRepository = bookRepository;
        }

        [HttpGet("[action]")]
        public ApiResult<IEnumerable<Book>> Books()
        {
            return ApiResult<IEnumerable<Book>>.FromSuccess(_bookRepository.GetAllBooks());
        }

        [HttpPost("[action]")]
        public ApiResult<Book> EditBook([FromBody]Book book)
        {
            var bookValidator = new BookValidator();
            var validationResult = bookValidator.Validate(book);

            if (validationResult.IsValid)
            {
                var editResult = _bookRepository.TryEditBook(book);
                return editResult ? ApiResult<Book>.FromSuccess(book) : ApiResult<Book>.FromError("Server error");
            }
            return ApiResult<Book>.FromError(validationResult.Errors.Select(x => x.ErrorMessage));
        }

        [HttpPost("[action]")]
        public ApiResult<Book> InsertBook([FromBody] Book book)
        {
            var bookValidator = new BookValidator();
            var validationResult = bookValidator.Validate(book);


            if (validationResult.IsValid)
            {
                var insertResult = _bookRepository.TryInsertBook(book);
                return insertResult ? ApiResult<Book>.FromSuccess(book) : ApiResult<Book>.FromError("Server error");
            }
            return ApiResult<Book>.FromError(validationResult.Errors.Select(x => x.ErrorMessage));
        }

        [HttpDelete("[action]")]
        public ApiResult<bool> DeleteBook(int id)
        {
            _bookRepository.DeleteBook(id);
            return ApiResult<bool>.FromSuccess(true);
        }
    }
}
